# Termico - Version GPU: Tegra k1

## Descripción:

Adaptación para leer las distintas zonas térmicas de Tegra K1:

CPU-therm: Tsensor inside TK1, close to CPU part. 
GPU-therm: Tsensor inside TK1, close to GPU part. 
MEM-therm: Tsensor inside TK1, close to MEM part. 
PLL-therm: Tsensor inside TK1, close to PLL part. 
Tboard_tegra: Tsensor inside external temperatur sensor, take the local temperature as PCB temperature. 
Tdiode_tegra: External temperature sensor read the temperature through Tdoide inside TK1, Tdiode is close to CPU and GPU part. it's more accurate than TK1 internal Tsensor readout. 


## Compilacion:

```
gcc termico.c -o termico
```

