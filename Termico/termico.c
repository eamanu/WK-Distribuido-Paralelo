//en K1 son 6 zonas
//cat /sys/devices/virtual/thermal/thermal_zone*/type
//cat /sys/devices/virtual/thermal/thermal_zone*/temp

//watch /sys/devices/virtual/thermal/thermal_zone*/temp
/*

type
CPU-therm: Tsensor inside TK1, close to CPU part.
GPU-therm: Tsensor inside TK1, close to GPU part.
MEM-therm: Tsensor inside TK1, close to MEM part.
PLL-therm: Tsensor inside TK1, close to PLL part.
Tboard_tegra: Tsensor inside external temperatur sensor, take the local temperature as PCB temperature. 
Tdiode_tegra: External temperature sensor read the temperature through Tdoide inside TK1, 
              Tdiode is close to CPU and GPU part. it's more accurate than TK1 internal Tsensor readout.

*/

//gcc termico.c -o termico

#include <stdio.h>   /* for FILE, fopen, fread, fclose, printf and sprintf */
#include <stdlib.h>  /* for atof */


int main (int argc, char **argv) {
   FILE *filePtr;
   char therm_path[64];  /* 64 bytes buffer for building current pseudo file path */
   char readBuf[16];     /* 16 bytes buffer for reading ASCII encoded value from pseudo file */
   float temp;   

   unsigned int curZone;
      
   for (curZone = 0; curZone < 6; curZone++) { //modificacion, curZone < 6
      unsigned int readBytes = 0;
      sprintf(therm_path, "/sys/devices/virtual/thermal/thermal_zone%u/temp", curZone);
      printf ("Reading %s:   ", therm_path);
      filePtr = fopen(therm_path, "r");
      if (!filePtr) {
    	printf ("Error, failed to open file\n");
    	continue;
      }
      while (readBytes < 16) {
         int curRead = fread(readBuf + readBytes, 1, 16 - readBytes, filePtr);
          if (curRead > 0)
    	     readBytes += curRead;
          else 
             break; /* nothing more to be read */
      }
      fclose (filePtr);

      if (readBytes > 0)
         *(readBuf + readBytes - 1) = 0; /* Remove last char '\n' */
      printf("[%s]  ", readBuf);

      temp = atof(readBuf);
      temp /= 1000.f;
      printf("%.02f\n", temp);
   }

   exit(0);
}
