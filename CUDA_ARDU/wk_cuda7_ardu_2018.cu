//    wk_cuda7_ardu_2018.cu: 
//    Algoritmo WK - Version para GPU instrumentada para comunicacion con ARDUINO
//    el proposito de esta version es la medicion del consumo de Potencia de Wk_cuda7
//    se utiliza un hardware propio basando en ARDUINO UNO y el Sensor de efecto Hall: ACS712
//    En esta version se instrumenta la interpolacion de Stolt

//    Copyright (C) 2018  javier nicolas uranga

/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


//mail de contacto: javiercba@gmail.com


//nvcc -arch=sm_32 wk_cuda7_ardu_2018.cu -o wk_cuda7_ardu_6144 -lcufft -DNslow=6144 -DNfast=6144 -DLAB=1 -DTIME=0 -DWRITE=0

//--------arduino-------------


#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <sys/ioctl.h>

//---------------------
#include <stdio.h>
#include <cufft.h>
#include <math.h>
#include <sys/time.h> 

#include <thrust/reduce.h>
#include <thrust/copy.h>
#include <thrust/device_ptr.h> 

#include "helper_cuda.h"

//----------------------

#define _Fc     9.4e9	//9400000000.0 = 9.4e9
#define _Ks     10.0e12 //10000000000000.0 = 10.0e12 
#define _Ffast  120.0e6	//120000000.0 = 120.0e6
#define _Fslow  6.0e2   //600.0 = 6.0e2

#define _V  250.0
#define _R0 27000

//-----------------------

#define _C  3.0e8	//300000000.0 = 3.0e8
#define _PI 3.1416


//----------------------------------


#ifndef WRITE
#define WRITE 1
#endif

#ifndef Nslow
#define Nslow 1024
#endif

#ifndef Nfast
#define Nfast 1024
#endif

//Ni = 8/2 -> se toma la mitad del kernel
#define Ni 4 


//--------------------


#ifndef LAB
#define LAB 1
#endif  

#ifndef TIME
#define TIME 0
#endif


#define MAX(a,b)((a>b)?a:b)

//-----def Arduino---------

int fd; 
struct termios toptions;
//double IDLE;
//void wait_idle();

//----------------------------------------------- kerneles ------------------------------------------------




__device__ cuComplex dSum_reduce_debug;
__device__ double dFc;
__device__ double dRfmC3;
__device__ double dRfmC2;
__device__ double dRfmC1;

__device__ double dFfast;

__device__ int dLim;
__device__ double dPI = 3.1416;
__device__ double d2PI = 6.2832;


__device__ double sinc_func(double x)
{
    return (sin(dPI*x + 1e-32))/(dPI*x + 1e-32);  //+1e-64 es el "eps" de matlab, para evitar la division por cero, durante truncamientos
}



//----------------------------debug --------------------------------------------

//IN:cuComplex *iodata1, cuComplex *idata2, int nx, int ny
//OUT:iodata1

__global__ void resta_debug_kernel(cuComplex *iodata1, cuComplex *idata2, int nx, int ny)
{

	int i = threadIdx.y + (blockIdx.y * blockDim.y);
  	int j = threadIdx.x + (blockIdx.x * blockDim.x);

	if(i < nx && j < ny)
	{
		iodata1[i*nx+j].x =  iodata1[i*nx+j].x - idata2[i*nx+j].x;
		iodata1[i*nx+j].y =  iodata1[i*nx+j].y - idata2[i*nx+j].y;
	}


}



__global__ void reduce_debug_Kernel(cuComplex *idata, int nx, int ny) {

	__shared__ cuComplex block_reduce;


	
  	int i = threadIdx.x + (blockIdx.x * blockDim.x);// id hilo global
	int j = threadIdx.y + (blockIdx.y * blockDim.y);// id hilo global

	int tidx = threadIdx.x; // id hilo dentro del bloque	
	int tidy = threadIdx.y; // id hilo dentro del bloque

	if(i < nx && j < ny)
	{
		if (i==0 && j==0) {// tarea del 1er hilo global de la grilla
			dSum_reduce_debug.x = 0.0f;
			dSum_reduce_debug.y = 0.0f;
		}

		if (tidx==0 && tidy==0) {// tarea del 1er hilo del bloque
			block_reduce.x = 0.0f;
			block_reduce.y = 0.0f;
		}
		__syncthreads();
		atomicAdd(&block_reduce.x, idata[i*nx+j].x);
		atomicAdd(&block_reduce.y, idata[i*nx+j].y);
		__syncthreads();

		if (tidx==0 && tidy==0) {// tarea del 1er hilo del bloque
			atomicAdd(&dSum_reduce_debug.x, block_reduce.x);
			atomicAdd(&dSum_reduce_debug.y, block_reduce.y);
		}

	}

}

//---------------------------fft------------------------------------------------

__global__ void init_kernel(cuComplex *iodata, int nx, int ny)
{

	int i = threadIdx.y + (blockIdx.y * blockDim.y);
  	int j = threadIdx.x + (blockIdx.x * blockDim.x);

	if(i < nx && j < ny)
	{
		iodata[i*nx+j].x =  0.0;
		iodata[i*nx+j].y =  0.0;
	}

}

//unicamente matrices cuadradas y pares
__global__ void fftshift_kernel(cuComplex *d_matrix, int filas, int cols)
{
	int fila = threadIdx.y + (blockIdx.y * blockDim.y);
  	int col = threadIdx.x + (blockIdx.x * blockDim.x);
	cuComplex  aux;
	if(fila < filas && col < cols){
		if(fila<filas/2)
		{
			if(col<cols/2)
			{
				aux = d_matrix[fila*cols + col];
				d_matrix[fila*cols + col] = d_matrix[(fila+(filas/2))*cols + (col+(cols/2))];
				d_matrix[(fila+(filas/2))*cols + (col+(cols/2))] = aux;
			}
			else
			{
				aux = d_matrix[fila*cols + col];
				d_matrix[fila*cols + col] = d_matrix[(fila+(filas/2))*cols + (col-(cols/2))];
				d_matrix[(fila+(filas/2))*cols + (col-(cols/2))] = aux ;
			}
		}

	}

}

__global__ void normalizar_ifft_kernel (cuComplex *odata, int nx, int ny)
{

	int i = threadIdx.y + (blockIdx.y * blockDim.y);
  	int j = threadIdx.x + (blockIdx.x * blockDim.x);

	if(i < nx && j < ny)
	{

		odata[i*nx+j].x =  odata[i*nx+j].x / ( double ) ( nx * nx );
		odata[i*nx+j].y =  odata[i*nx+j].y / ( double ) ( nx * ny );
	}

}



//NUEVO ahora solo INPLACE 31/10/2017
//S1 es entrada, pero tambien la salida
__global__ void rfm_S2_kernel (cuComplex *S1, double *fslow, int nx, double *ffast, int ny){


	int i = threadIdx.y + (blockIdx.y * blockDim.y);
  	int j = threadIdx.x + (blockIdx.x * blockDim.x);

	double res = 0.0;

	double s1x=0.0;
	double s1y=0.0;
	double rfmx=0.0;
	double rfmy=0.0;
	double root = 0.0;

	if(i < nx && j < ny)
	{      
		//construccion de rfm

		//root = (double) ( (dFc+ffast[j])*(dFc+ffast[j]) - dRfmC1 * (fslow[i]*fslow[i]) ) ;
		root = (dFc+ffast[j])*(dFc+ffast[j]) - dRfmC1 * (fslow[i]*fslow[i]);

		res = dRfmC3 * sqrt(  root  ) + dRfmC2 * (ffast[j]*ffast[j]);
		// toma modulo 2*pi
		res = fmod(res, d2PI);
		//calcula la exponencial compleja
		rfmx=cos(res);
		rfmy=sin(res);
		//cargo S1 (es S luego de las FFT)
		s1x = S1[i*ny+j].x;
		s1y = S1[i*ny+j].y;
		//aplica rfm a S1 y obtengo S2 INPLACE!!!!!, se pisa S1

		S1[i*ny+j].x = ( s1x * rfmx ) - ( s1y * rfmy);
		S1[i*ny+j].y = ( s1x * rfmy ) + ( s1y * rfmx);

	}

}



//--------------------------------------------------------


//creacion de la nueva variable deltaFfast que usara Stolt para inteporlar con S2
__global__ void delta_ffast_kernel (double *out_delta_ffast, double *fslow, int nx, double *ffast, int ny){

	int i = threadIdx.y + (blockIdx.y * blockDim.y);//filas
  	int j = threadIdx.x + (blockIdx.x * blockDim.x);//columas
	
	double res = 0.0;
	double root = 0.0;

	
	if(i < nx && j < ny)//filas && columnas
	{      
		root = (double) ( (dFc+ffast[j])*(dFc+ffast[j]) - dRfmC1 * (fslow[i]*fslow[i])  );
		res = sqrt( root ) - (dFc+ffast[j]);

		//normaliza deltafasta a valores de indice mutiplicando por Nfast/Ffast
		//Nfast es ny
		res = res*(ny/dFfast);

		out_delta_ffast[i*ny+j]=res;
	}

}

//----------------------------------------------------------

__global__ void deltaffast_stolt_kernel (cuComplex *out_stolt_S3, cuComplex *S2, double *fslow, int filas, double *ffast, int cols){


    	int ix = threadIdx.x + (blockIdx.x * blockDim.x);//columnas
    	int iy = threadIdx.y + (blockIdx.y * blockDim.y); //indice de filas
    
    	//declaracion e inicializacion de variables para el calculo
    	
    	//double data_delta_ffast = 0.0f;
    
    	double res = 0.0;
    	double root = 0.0;
    	//---------------------
    
    	cuComplex valor_ant_3   = make_cuComplex(0.0f, 0.0f);
    	cuComplex valor_ant_2   = make_cuComplex(0.0f, 0.0f);
    	cuComplex valor_ant_1   = make_cuComplex(0.0f, 0.0f);
        cuComplex valor_central = make_cuComplex(0.0f, 0.0f);
        cuComplex valor_pos_1   = make_cuComplex(0.0f, 0.0f);
        cuComplex valor_pos_2   = make_cuComplex(0.0f, 0.0f);
        cuComplex valor_pos_3   = make_cuComplex(0.0f, 0.0f);
        cuComplex valor_pos_4   = make_cuComplex(0.0f, 0.0f);

        int ent=0;
       	double dec= 0.0f;

       	double sinc_ant_3 = 0.0f;
       	double sinc_ant_2 = 0.0f;
       	double sinc_ant_1 = 0.0f;
       	double sinc_central= 0.0f;
     	double sinc_pos_1 = 0.0f;
       	double sinc_pos_2 = 0.0f;
     	double sinc_pos_3 = 0.0f;
       	double sinc_pos_4 = 0.0f;

    	int lim1 = Ni+dLim;    
    	int lim2 = cols-lim1; 
    
    
    	if ( (iy < filas)  && ( lim1-1 <= ix) && (ix <= lim2-1) )
    	{
    		//calculo del delta_ffast al vuelo
    
    		root = (double) ( (dFc+ffast[ix])*(dFc+ffast[ix]) - dRfmC1 * (fslow[iy]*fslow[iy])  );
    		res = sqrt( root ) - (dFc+ffast[ix]);
    		res = res*(cols/dFfast);
    	
    		//------------------------------
    
    		//valor double
    		//data_delta_ffast = deltafast[iy * cols + ix];
    		//data_delta_ffast = res;
    
    		//ent= (int) data_delta_ffast;
    		ent= (int) res;
    
    		valor_ant_3   = S2[iy*cols + (ix+ent-3)]; //fila*Columnas+columna
    		valor_ant_2   = S2[iy*cols + (ix+ent-2)];
    		valor_ant_1   = S2[iy*cols + (ix+ent-1)];
        	valor_central = S2[iy * cols+(ix+ent)];

        	valor_pos_1   = S2[iy*cols + (ix+ent+1)];
        	valor_pos_2   = S2[iy*cols + (ix+ent+2)];
    		valor_pos_3   = S2[iy*cols + (ix+ent+3)];
    		valor_pos_4   = S2[iy*cols + (ix+ent+4)];
    
    
           		//dec= data_delta_ffast - ent; 
    		dec= res - ent; 

       		sinc_ant_3   = sinc_func(dec-3.0);
       		sinc_ant_2   = sinc_func(dec-2.0);
       		sinc_ant_1   = sinc_func(dec-1.0);

       		sinc_central = sinc_func(dec); //+1e-64: para evitar la division por cero en la sinc

       		sinc_pos_1   = sinc_func(dec+1.0);
       		sinc_pos_2   = sinc_func(dec+2.0);
       		sinc_pos_3   = sinc_func(dec+3.0);
       		sinc_pos_4   = sinc_func(dec+4.0);
		
    
    		out_stolt_S3[iy * cols + ix].x = 	  (valor_ant_3.x   *  sinc_ant_3) 
    						+ (valor_ant_2.x   *  sinc_ant_2)  
    						+ (valor_ant_1.x   *  sinc_ant_1) 
    						+ (valor_central.x *  sinc_central) 
    						+ (valor_pos_1.x   *  sinc_pos_1) 
    						+ (valor_pos_2.x   *  sinc_pos_2)
    						+ (valor_pos_3.x   *  sinc_pos_3) 
    						+ (valor_pos_4.x   *  sinc_pos_4);
    
    
    		out_stolt_S3[iy * cols + ix].y =    (valor_ant_3.y   *  sinc_ant_3) 
    						+ (valor_ant_2.y   *  sinc_ant_2) 
    						+ (valor_ant_1.y   *  sinc_ant_1) 
    						+ (valor_central.y *  sinc_central) 
    						+ (valor_pos_1.y   *  sinc_pos_1) 
    						+ (valor_pos_2.y   *  sinc_pos_2)
    						+ (valor_pos_3.y   *  sinc_pos_3) 
    						+ (valor_pos_4.y   *  sinc_pos_4);


	}//del if


}




//------------------------------------------wrappers DEBUG -------------------------------------------



__host__ int resta_debug (cuComplex *d_in_out1, cuComplex *d_in2, int nx, int ny){

    
        dim3 Nthreads(32,32,1);
        dim3 Nblocks((ny/Nthreads.y) + (ny % Nthreads.y?1:0), (nx/Nthreads.x) + (nx % Nthreads.x?1:0));
    
    	if(LAB){
    		//printf("  : \n");
    		//getchar();
    		resta_debug_kernel<<<Nblocks, Nthreads>>>(d_in_out1, d_in2, nx, ny);
    	
    	}

    	if(TIME)
    	{
    		cudaEvent_t start, stop;

    		cudaEventCreate(&start);
    		cudaEventCreate(&stop);

		cudaEventRecord(start);
    		resta_debug_kernel<<<Nblocks, Nthreads>>>(d_in_out1, d_in2, nx, ny);
		cudaEventRecord(stop);

		cudaEventSynchronize(stop);
		float milisec=0;
		cudaEventElapsedTime(&milisec, start, stop);


		printf("medicion tiempo kernel resta_debug: %f milisegundos \n", milisec);

		cudaEventDestroy(start);
		cudaEventDestroy(stop);

	    }


    	getLastCudaError("resta_debug_kernel() kernel failed");

    	checkCudaErrors( cudaDeviceSynchronize() );

    	return 1;

}


__host__ int reduce_debug (cuComplex *d_idata, int nx, int ny){


    dim3 Nthreads(32,32,1);
    dim3 Nblocks((ny/Nthreads.y) + (ny % Nthreads.y?1:0), (nx/Nthreads.x) + (nx % Nthreads.x?1:0));

    reduce_debug_Kernel<<<Nblocks, Nthreads>>>(d_idata, nx, ny);
    getLastCudaError("reduce_debug_Kernel() kernel failed");

    checkCudaErrors( cudaDeviceSynchronize() );

    return 1;

}

//------------------------------------------wrappers -------------------------------------------



// __host__
__host__ int fftshift(cuComplex *d_matrix, int filas, int cols)
{

    	dim3 Nthreads(32,32,1); 	
 
    	dim3 Nblocks((cols/Nthreads.x) + (cols % Nthreads.x?1:0), (filas/Nthreads.y) + (filas % Nthreads.y?1:0));


    	if(LAB){
    		printf("fftshift  : \n");
    		//getchar();
    		fftshift_kernel<<<Nblocks, Nthreads>>>(d_matrix, filas, cols);
    	
    	}
       	if(TIME)
        {
        		cudaEvent_t start, stop;
    
        		cudaEventCreate(&start);
        		cudaEventCreate(&stop);
    
    		    cudaEventRecord(start);
        		fftshift_kernel<<<Nblocks, Nthreads>>>(d_matrix, filas, cols);
    		    cudaEventRecord(stop);
    
        		cudaEventSynchronize(stop);
        		float milisec=0;
        		cudaEventElapsedTime(&milisec, start, stop);
        
        
        		printf("medicion tiempo fftshift_kernel: %f milisegundos \n", milisec);
        
        		cudaEventDestroy(start);
        		cudaEventDestroy(stop);
    
    	}


    	getLastCudaError("fftshift_kernel() kernel failed");

    	checkCudaErrors( cudaDeviceSynchronize() );

    	return 1;

}


__host__ int normalizarIfft(cuComplex *d_matrix_out, int nx, int ny)
{

    	dim3 Nthreads(32,32,1);
 
    	dim3 Nblocks((nx/Nthreads.x) + (nx % Nthreads.x?1:0), (ny/Nthreads.y) + (ny % Nthreads.y?1:0));

    	if(LAB){
    		printf(" normalizar : \n");
    		//getchar();
    		normalizar_ifft_kernel<<<Nblocks, Nthreads>>>(d_matrix_out, nx, ny);
    	}
    	if(TIME)
    	{
    		cudaEvent_t start, stop;

    		cudaEventCreate(&start);
    		cudaEventCreate(&stop);

		    cudaEventRecord(start);
    		normalizar_ifft_kernel<<<Nblocks, Nthreads>>>(d_matrix_out, nx, ny);
		    cudaEventRecord(stop);

		    cudaEventSynchronize(stop);
    		float milisec=0;
    		cudaEventElapsedTime(&milisec, start, stop);


    		printf("medicion tiempo normalizar_ifft_kernel: %f milisegundos \n", milisec);
    
    		cudaEventDestroy(start);
    		cudaEventDestroy(stop);

	    }

    	getLastCudaError("normalizar_ifft_kernel() kernel failed");

    	checkCudaErrors( cudaDeviceSynchronize() );

    	return 1;

}



//NUEVO  31/10/2017, ahora es inplace
__host__ int rfm_S2 (cuComplex *d_in_S1, double *d_in_fslow, int nx, double *d_in_ffast, int ny){

    	dim3 Nthreads(32,32,1);
    	dim3 Nblocks((ny/Nthreads.y) + (ny % Nthreads.y?1:0), (nx/Nthreads.x) + (nx % Nthreads.x?1:0));

    	if(LAB){
    		printf("rfms2  : \n");
    		//getchar();
    		rfm_S2_kernel<<<Nblocks, Nthreads>>>(d_in_S1, d_in_fslow, nx, d_in_ffast, ny);
    	
    	}

    	if(TIME)
    	{
    		cudaEvent_t start, stop;

    		cudaEventCreate(&start);
    		cudaEventCreate(&stop);

		    cudaEventRecord(start);
    		rfm_S2_kernel<<<Nblocks, Nthreads>>>(d_in_S1, d_in_fslow, nx, d_in_ffast, ny);
		    cudaEventRecord(stop);

		    cudaEventSynchronize(stop);
		    float milisec=0;
		    cudaEventElapsedTime(&milisec, start, stop);


		    printf("medicion tiempo rfm_S2_kernel: %f milisegundos \n", milisec);

		    cudaEventDestroy(start);
		    cudaEventDestroy(stop);

     	}

    
    	getLastCudaError("rfm_S2_kernel() kernel failed");

    	checkCudaErrors( cudaDeviceSynchronize() );

    	return 1;

}



//----------------------------------------------------------------------------------------

__host__ int delta_ffast(double *d_out_delta_ffast, double *d_in_fslow, int nx, double *d_in_ffast, int ny){

    	dim3 Nthreads(32,32,1);
    	dim3 Nblocks((nx/Nthreads.x) + (nx % Nthreads.x?1:0), (ny/Nthreads.y) + (ny % Nthreads.y?1:0));

    	if(LAB){
		printf("deltafast  : \n");
		//getchar();
		delta_ffast_kernel<<<Nblocks, Nthreads>>>(d_out_delta_ffast, d_in_fslow, nx, d_in_ffast, ny);
	
	    }
	    
    	if(TIME)
    	{
    		cudaEvent_t start, stop;

    		cudaEventCreate(&start);
    		cudaEventCreate(&stop);

		    cudaEventRecord(start);
    		delta_ffast_kernel<<<Nblocks, Nthreads>>>(d_out_delta_ffast, d_in_fslow, nx, d_in_ffast, ny);
		    cudaEventRecord(stop);

		    cudaEventSynchronize(stop);
		    float milisec=0;
		    cudaEventElapsedTime(&milisec, start, stop);


		    printf("medicion tiempo delta_ffast_kernel: %f milisegundos \n", milisec);

		    cudaEventDestroy(start);
		    cudaEventDestroy(stop);

    	}

    	getLastCudaError("delta_ffast_kernel() kernel failed");

    	checkCudaErrors( cudaDeviceSynchronize() );

    	return 1;
}



__host__ int deltaffast_stolt(cuComplex *d_out_stolt_S3, cuComplex *d_in_S2, double *d_fslow, int filas, double *d_ffast,  int cols){
    
    dim3 Nthreads(16,16,1);

    dim3 Nblocks((filas/Nthreads.x) + (filas % Nthreads.x?1:0), (cols/Nthreads.y) + (cols % Nthreads.y?1:0));

   if(LAB){

		printf("***************LAB: Stolt : *************************\n");
		char buf[64] = "AAAAAAA";
		//printf("Stolt: enviar comando inicio amperimetro \n");
		write(fd, "0",1); // inicia: envia 0, arduino recibe 48 en ascii

		deltaffast_stolt_kernel<<<Nblocks, Nthreads>>>(d_out_stolt_S3, d_in_S2, d_fslow, filas, d_ffast, cols);
		
		write(fd, "1",1); // termina: envia 1, arduino recive 49 en ascii

		sleep(0.1);		
		int n = read(fd, buf, 64);
		buf[n] = 0;
		printf("valor corriente Stolt: %s \n", buf);

		printf("***************LAB: Fin Stolt : *************************\n");
    }
    
    if(TIME)
    {
    		cudaEvent_t start, stop;

    		cudaEventCreate(&start);
    		cudaEventCreate(&stop);

		    cudaEventRecord(start);
    		deltaffast_stolt_kernel<<<Nblocks, Nthreads>>>(d_out_stolt_S3, d_in_S2, d_fslow, filas, d_ffast, cols);
		    cudaEventRecord(stop);

		    cudaEventSynchronize(stop);
		    float milisec=0;
		    cudaEventElapsedTime(&milisec, start, stop);


		    printf("medicion tiempo deltaffast_stolt_kernel: %f milisegundos \n", milisec);

		    cudaEventDestroy(start);
		    cudaEventDestroy(stop);

    }


    getLastCudaError("deltaffast_stolt_kernel() kernel failed");
    checkCudaErrors( cudaDeviceSynchronize() );

    return 1;
}

//cuFFT: 
__host__ int fft(cuComplex *d_S, int nx, int ny){


	cufftHandle plan;
	cufftPlan2d(&plan, nx, ny, CUFFT_C2C);

	if(LAB){
		printf("FFT  : \n");
		//getchar();
		cufftExecC2C(plan, d_S, d_S, CUFFT_FORWARD);//inplace
	
	}
	if(TIME){

		    cudaEvent_t start, stop;

    		cudaEventCreate(&start);
    		cudaEventCreate(&stop);

		    cudaEventRecord(start);

		    cufftExecC2C(plan, d_S, d_S, CUFFT_FORWARD);

		    cudaEventRecord(stop);

		    cudaEventSynchronize(stop);
		    float milisec=0;
		    cudaEventElapsedTime(&milisec, start, stop);


		    printf("medicion tiempo fft2_kernel: %f milisegundos \n", milisec);

		    cudaEventDestroy(start);
		    cudaEventDestroy(stop);

	}
 
    getLastCudaError("cufftExecC2C CUFFT_FORWARD kernel failed");
    checkCudaErrors( cudaDeviceSynchronize() );

	//--------------------------------limpieza -----------------------

	cufftDestroy(plan);


    return 1;
}


//cuFFT
__host__ int ifft(cuComplex *d_out_stolt_S3, int nx, int ny){


   	cufftHandle plan2;
	cufftPlan2d(&plan2, nx, ny, CUFFT_C2C);

	if(LAB){
		printf("FFT inversa  : \n");
		//getchar();

		//lo ejecuta in-place, piso de nuevo a d_out_stolt_S3 !!!!
		cufftExecC2C(plan2, d_out_stolt_S3, d_out_stolt_S3, CUFFT_INVERSE);
	
	}
	if(TIME){

		    cudaEvent_t start, stop;

    		cudaEventCreate(&start);
    		cudaEventCreate(&stop);

		    cudaEventRecord(start);

		    cufftExecC2C(plan2, d_out_stolt_S3, d_out_stolt_S3, CUFFT_INVERSE);

    		cudaEventRecord(stop);
    
    		cudaEventSynchronize(stop);
    		float milisec=0;
    		cudaEventElapsedTime(&milisec, start, stop);
    
    
    		printf("medicion tiempo ifft2_kernel: %f milisegundos \n", milisec);
    
    		cudaEventDestroy(start);
    		cudaEventDestroy(stop);
	}

    getLastCudaError("cufftExecC2C CUFFT_INVERSE kernel failed");
    checkCudaErrors( cudaDeviceSynchronize() );

	//--------------------------------limpieza -----------------------

	cufftDestroy(plan2);

    return 1;
}


//Thrust
__host__ double minimo(double *d_out_delta_fast, int nx, int ny, double cteThrust)
{

	thrust::device_ptr<double> d_t = thrust::device_pointer_cast(d_out_delta_fast);
	double minim=0.0;

	if(LAB){
		printf("minimo  : \n");
		//getchar();
		minim = thrust::reduce(d_t, d_t+(Nslow*Nfast), cteThrust, thrust::minimum<double>());
	
	}
	if(TIME){

			
		    cudaEvent_t start, stop;

    		cudaEventCreate(&start);
    		cudaEventCreate(&stop);

    		cudaEventRecord(start);
    
    		minim = thrust::reduce(d_t, d_t+(nx*ny), cteThrust, thrust::minimum<double>());
    
    		cudaEventRecord(stop);
    
    		cudaEventSynchronize(stop);
    		float milisec=0;
    		cudaEventElapsedTime(&milisec, start, stop);


    		printf("medicion tiempo minimo_reduce_kernel: %f milisegundos \n", milisec);
    
    		cudaEventDestroy(start);
    		cudaEventDestroy(stop);

	}

	getLastCudaError("minimo thrust::minimo reduce kernel failed");
	checkCudaErrors( cudaDeviceSynchronize() );

	return minim;

}


//Thrust
__host__ double maximo(double *d_out_delta_fast, int nx, int ny, double cteThrust)
{

	thrust::device_ptr<double> d_t = thrust::device_pointer_cast(d_out_delta_fast);
	double maxim=0.0;

	if(LAB){
		printf("maximo  : \n");
		//getchar();
		maxim = thrust::reduce(d_t, d_t+(Nslow*Nfast), cteThrust, thrust::maximum<double>());
	
	}

	if(TIME){

			
		    cudaEvent_t start, stop;

    		cudaEventCreate(&start);
    		cudaEventCreate(&stop);

    		cudaEventRecord(start);
    
    		maxim = thrust::reduce(d_t, d_t+(nx*ny), cteThrust, thrust::maximum<double>());
    
    		cudaEventRecord(stop);
    
    		cudaEventSynchronize(stop);
    		float milisec=0;
    		cudaEventElapsedTime(&milisec, start, stop);
    
    
    		printf("medicion tiempo maximo_reduce_kernel: %f milisegundos \n", milisec);
    
    		cudaEventDestroy(start);
		    cudaEventDestroy(stop);

	}

	getLastCudaError("minimo thrust::maximo reduce kernel failed");
	checkCudaErrors( cudaDeviceSynchronize() );

	return maxim;

}
//------------------ Utils --------------------------

__host__ int init(cuComplex *d_matrix_out, int nx, int ny)
{

    	dim3 Nthreads(32,32,1);
 
    	dim3 Nblocks((nx/Nthreads.x) + (nx % Nthreads.x?1:0), (ny/Nthreads.y) + (ny % Nthreads.y?1:0));

    	if(LAB){
    		printf("init  : \n");
    		//getchar();
    		init_kernel<<<Nblocks, Nthreads>>>(d_matrix_out, ny, ny);
    	
    	}
    	if(TIME)
    	{
    		cudaEvent_t start, stop;

    		cudaEventCreate(&start);
    		cudaEventCreate(&stop);

		    cudaEventRecord(start);
    		init_kernel<<<Nblocks, Nthreads>>>(d_matrix_out, ny, ny);
		    cudaEventRecord(stop);

    		cudaEventSynchronize(stop);
    		float milisec=0;
    		cudaEventElapsedTime(&milisec, start, stop);
    
    
    		printf("medicion tiempo init_kernel: %f milisegundos \n", milisec);
    
    		cudaEventDestroy(start);
    		cudaEventDestroy(stop);

    	}


    	getLastCudaError("init_kernel() kernel failed");

    	checkCudaErrors( cudaDeviceSynchronize() );

    	return 1;

}

__host__ void init_mat_host(cuComplex *h_out, int nx, int ny)
{
	int i,j;

	for (i=0; i<nx; i++)
	{
	    for(j=0; j<ny; j++)
	    {
	       h_out[i*ny+j].x=0.0; 
	       h_out[i*ny+j].y=0.0;
	    }
	}
	return;
}



//nuevo
__host__ int linspace (double* vector, double minval, double maxval, int n)
{

	 if(n <2){
	    return 0;
	 }
	
	 int i = 0;
	 double step = (maxval-minval)/(floor((double)n) - 1.0);

	 for (i = 0; i < n; i++)
	 {
	    vector[i]= minval + i*step;

	 }

	 return 1;

}


__host__ int readMat2(char arch_re[], char arch_im[], cuComplex* h_mat, int nx, int ny)
{
 
    int i,j;
    double re;
    double imag;


    FILE *pfile_re, *pfile_im;
    pfile_re = fopen(arch_re, "r");
    pfile_im = fopen(arch_im, "r");

    if (pfile_re == NULL)
    {
       printf("error al abrir archivo para lectura %s \n", arch_re);
       return (-1);
    }
    if (pfile_im == NULL)
    {
       printf("error al abrir archivo para lectura %s \n", arch_im);
       return (-1);
    }

    printf("leyendo archivo: %s \n", arch_re);
    printf("leyendo archivo: %s \n", arch_im);

    for (i = 0; i < nx; i++)
    {
       for(j = 0; j < ny; j++)
       {
         fscanf(pfile_re, "%lf,",&re);
	 fscanf(pfile_im,"%lf,",&imag);

	 h_mat[i*ny+j]= make_cuComplex(re, imag);
       }

    }


   fclose(pfile_re);
   fclose(pfile_im);


   return 1;
}


__host__ int saveMat_Parts(cuComplex *h_data, int nx, int ny, char re_arch[], char im_arch[])
{

    int i,j;

    FILE *pfile;
    pfile = fopen(re_arch, "w");


    if (pfile == NULL)
    {
       printf("error al abrir archivo para escritura %s \n", re_arch);
       return (-1);
    }

    printf("Escribiendo archivo: %s \n", re_arch);


    for (i = 0; i < nx; i++)
    {
       for(j = 0; j < ny; j++)
       {
		if(j!=ny-1){

			   fprintf(pfile,"%lf,", h_data[(i * ny) + j].x);
			   
		}else{      //el ultimo caso no termina con ","

			   fprintf(pfile,"%lf", h_data[(i * ny) + j].x);
		
		}
       }
       fprintf(pfile,"\n");
    }

    fclose(pfile);

    //-----------------------------------

    pfile = fopen(im_arch, "w");


    if (pfile == NULL)
    {
       printf("error al abrir archivo para escritura %s \n", im_arch);
       return (-1);
    }

    printf("Escribiendo archivo: %s \n", im_arch);


    for (i = 0; i < nx; i++)
    {
       for(j = 0; j < ny; j++)
       {
		if(j!=ny-1){

			   fprintf(pfile,"%lf,", h_data[(i * ny) + j].y);
			   
		}else{      //el ultimo caso no termina con ","

			   fprintf(pfile,"%lf", h_data[(i * ny) + j].y);
		
		}
       }
       fprintf(pfile,"\n");
    }

    fclose(pfile);

  return 0;


}



__host__ int saveVec(double *h_data, int nx, char arch[])
{

    int i;

    FILE *pfile;
    pfile = fopen(arch, "w");


    if (pfile == NULL)
    {
       printf("error al abrir archivo para escritura %s \n", arch);
       return (-1);
    }

    printf("Escribiendo archivo: %s \n", arch);


    for (i = 0; i < nx; i++)
    {
	if(i!=nx-1){

	   fprintf(pfile,"%lf,", h_data[i]);

	}else{

	   fprintf(pfile,"%lf", h_data[i]);
	
       }
       fprintf(pfile,"\n");
    }

  fclose(pfile);

  return 0;


}


void config_Arduino()
{

	/* open serial port */
	fd = open("/dev/ttyACM0", O_RDWR | O_NOCTTY);
	printf("fd opened as %i\n", fd);
	/* wait for the Arduino to reboot */
	usleep(3500000); //son 3.5 segundos =3.500.000 microsegundos
	/* get current serial port settings */
	tcgetattr(fd, &toptions);
	/* set 9600 baud both ways */
	cfsetispeed(&toptions, B9600);
	cfsetospeed(&toptions, B9600);
	/* 8 bits, no parity, no stop bits */
	toptions.c_cflag &= ~PARENB;
	toptions.c_cflag &= ~CSTOPB;
	toptions.c_cflag &= ~CSIZE;
	toptions.c_cflag |= CS8;
	/* Canonical mode */
	toptions.c_lflag |= ICANON;
	/* commit the serial port settings */
	tcsetattr(fd, TCSANOW, &toptions);

}



//--------------------------------main WK--------------------------------------------------------------

int main(int argc, char *argv[])
{




	//-configuracion del puerto serie para comunicacion con Ardunio de forma canonica-

	if(LAB){
		config_Arduino();
		//IDLE = medir_idle();
	}
	//---------------------------------------------------------


	//int i;

	printf("Nslow: %d, Nfast:%d, R0:%d\n, presione cualquier tecla", Nslow, Nfast, _R0 );

	double Fc     = (double) _Fc;
	double Ks     = (double) _Ks; 	
	double Ffast  = (double) _Ffast;
	double Fslow  = (double) _Fslow;


	double V  = (double) _V;
	double R0 = (double) _R0;
	double C  = (double) _C;
	double PI = (double) _PI;



	//leer S desde el disco
	//aplicar ifftshift 2D a S
	//aplicar fft2D a S
	//aplicar ifftshift 2D a S
	//obtener S1 para empezar a trabajar con el algoritmo

    cuComplex *h_S =  (cuComplex *) malloc(  Nslow*Nfast * sizeof(cuComplex));  

	//Lectura de datos crudos



	char re_file[] = "re.txt"; 
	char im_file[] = "im.txt"; 

	
	readMat2(re_file, im_file, h_S, Nslow, Nfast);


	//*********************************************************************  

	cuComplex *d_S;

	checkCudaErrors( cudaMalloc((void**)&d_S, sizeof(cuComplex) * Nslow*Nfast) );

	checkCudaErrors( cudaMemcpy(d_S, h_S, Nslow*Nfast * sizeof(cuComplex), cudaMemcpyHostToDevice) );

	free(h_S);

	//------------------- iFFTSHIFT -------------solo matrices cuadradas-----------------

	fftshift(d_S, Nslow, Nfast);

	//------------------- FFT2 ---------inplace--------------------------------------------------
	
	fft(d_S, Nslow, Nfast);


	//------------------- FFTSHIFT ------------solo matrices cuadradas-----------

	//in-place 
	fftshift(d_S, Nslow, Nfast);


	//----------------- vectores ffast y fslow generadores de RFM -----------------------------------


    double *h_ffast  	    =  (double *) malloc( Nfast * sizeof(double));  
	double *h_fslow  	    =  (double *) malloc( Nslow * sizeof(double));  
        
	//vector fila

	double minval =0.0;
	double maxval =0.0;
	int n =0;

	if(LAB){
		//printf("linspace  : \n");
		//getchar();

		minval = -(Ffast/2);
		maxval =  (Ffast/2) - (Ffast/Nfast);
		n =Nfast;
		linspace(h_ffast,minval,maxval,n);  
		//vector columna
		minval =-(Fslow/2); 
		maxval = (Fslow/2) - (Fslow/Nslow);
		n =Nslow;
		linspace(h_fslow,minval,maxval,n); 
	
	}
	if(TIME){

		struct timeval linstart, linend;
		gettimeofday(&linstart, NULL);

		//--------------------------
		minval = -(Ffast/2);
		maxval =  (Ffast/2) - (Ffast/Nfast);
		n =Nfast;
		linspace(h_ffast,minval,maxval,n);  
		//vector columna
		minval =-(Fslow/2); 
		maxval = (Fslow/2) - (Fslow/Nslow);
		n =Nslow;
		linspace(h_fslow,minval,maxval,n); 
		//---------------------------------

		gettimeofday(&linend, NULL);
		printf("tiempo linspaces secuencial milisegundos:%ld\n", ((linend.tv_sec * 1000 + linend.tv_usec/1000) - (linstart.tv_sec * 1000 + linstart.tv_usec/1000)));

	}
	

	//-------------------------

	double *d_ffast;

	checkCudaErrors( cudaMalloc((void**)&d_ffast, sizeof(double) * Nfast) );
	checkCudaErrors( cudaMemcpy(d_ffast, h_ffast, Nfast * sizeof(double), cudaMemcpyHostToDevice) );


	double *d_fslow;
	checkCudaErrors( cudaMalloc((void**)&d_fslow, sizeof(double) * Nslow) );
	checkCudaErrors( cudaMemcpy(d_fslow, h_fslow, Nslow * sizeof(double), cudaMemcpyHostToDevice) );

	//-------------------limpieza --------------------------------
	//NO ELIMINAR todavia a d_S, se usan mas adelante
    free(h_ffast);
    free(h_fslow);

	//------------------- ctes para RFM-------------------------

	double h_RfmC1 = (C*C)/(4*V*V);
	double h_RfmC2 = PI/Ks;
	double h_RfmC3 = (4*PI*R0)/C;

	
	checkCudaErrors( cudaMemcpyToSymbol(dRfmC1,&h_RfmC1,sizeof(double)) );
	checkCudaErrors( cudaMemcpyToSymbol(dRfmC2,&h_RfmC2,sizeof(double)) );
	checkCudaErrors( cudaMemcpyToSymbol(dRfmC3,&h_RfmC3,sizeof(double)) );


	double h_Fc = Fc; 
	checkCudaErrors( cudaMemcpyToSymbol(dFc,&h_Fc,sizeof(double)) );


	//********************************** RFM- S2 DEBUG ***********************************


	rfm_S2 (d_S, d_fslow, Nslow, d_ffast, Nfast);

	//-------------------------a partir de aqui d_S es el teorico S2 -------------------



	//***************************************************************************************
	//********************************** FIN RFM - S2 DEBUG *********************************
	//***************************************************************************************




	//-------------------------constantes para Delta_fast------------------------------

	double h_Ffast = Ffast; //esta ok
	checkCudaErrors( cudaMemcpyToSymbol(dFfast,&h_Ffast,sizeof(double)) );

    //------------------------llamada al kernel de delta_fast --------------------------


	double *h_out_delta_fast2 =  (double *) malloc( 1 * sizeof(double));

	double *d_out_delta_fast;
    checkCudaErrors( cudaMalloc((void**)&d_out_delta_fast, sizeof(double) * Nslow*Nfast) );
	


	//kernel
	//IN=  d_fslow, Nslow, d_ffast, Nfast
	//OUT= d_out_delta_fast
	delta_ffast(d_out_delta_fast, d_fslow, Nslow, d_ffast, Nfast);


	checkCudaErrors( cudaMemcpy(h_out_delta_fast2, d_out_delta_fast, 1*sizeof(double), cudaMemcpyDeviceToHost) );



	//*********************************************************************************************
	//**************************THRUST---reduce max y min****memcpytosymbol************************
	//*********************************************************************************************

	double cteThrust2 = h_out_delta_fast2[0];


	//---------------------------------limpieza ------------------------------------------------

	free(h_out_delta_fast2);
	//------------------------------------------------------------------------------------------

	double minim = minimo(d_out_delta_fast, Nslow, Nfast,  cteThrust2);
	double maxim = maximo(d_out_delta_fast, Nslow, Nfast,  cteThrust2);


	int fixminim = (int) abs(minim);
	int fixmaxim = (int) abs(maxim);

    int fixmax_mm = MAX(fixmaxim,fixminim);
	
	checkCudaErrors( cudaMemcpyToSymbol(dLim,&fixmax_mm,sizeof(int)) );
	

	//nuevo
	checkCudaErrors( cudaFree(d_out_delta_fast) );

	//*******************************************************************************************
	//************************************ STOLT ***** out-place ********************************
	//*******************************************************************************************


	//creacion de la matriz de salida de stolt

	//cantidad en columnas: desde  Ni+fixmax_mm   hasta  Nfast-Ni-fixmax_mm

	//en realidad seria num_cols = (Nfast-Ni-fixmax_mm) - (Ni+fixmax_mm) =  Nfast-2*(Ni+fixmax_mm)
    //y num_rows=: Nslow

	
	int dimx = Nslow;
	int dimy = Nfast;

	
	cuComplex *d_out_stolt_S3;
	cudaMalloc((void**)&d_out_stolt_S3, sizeof(cuComplex) * dimx*dimy );

	init(d_out_stolt_S3, dimx, dimy);

	//llamada al kernel de stolt

	//observ: aqui se debe considerar a d_S = S2 de Matlab
	//IN: double: d_out_delta_fast,  cuComlpex: d_S, int: Nslow, Nfast
	//OUT: cuComplex: d_out_stolt_S3

	//observ: Stolt no puede trabajar in-place sobre d_S poruqe alteraria los resultados

	deltaffast_stolt(d_out_stolt_S3, d_S, d_fslow, Nslow, d_ffast, Nfast);



	//-------------------------------limpieza ----------------------------------------------

	//No eliminar a h_out_stolt_S3,  d_out_stolt_S3 se usa mas adelante
	checkCudaErrors( cudaFree(d_S) ); //se libera S2
	

	//NUEVO

	checkCudaErrors( cudaFree(d_ffast) );
	checkCudaErrors( cudaFree(d_fslow) );
		
	//*********************************************************************************************
	//**********************************   FIN STOLT   ********************************************
	//*********************************************************************************************



	//-------i-FFTSHIFT (es igual a la fftshift para matrices cuadradas pares ) -----------------------

	//para el caso de matrices cuadradas se cumple que i-FFTSHIFT = FFTSHIFT, lo hace in-place

	//piso in-place a d_out_stolt_S3 !
	//IN  = d_out_stolt_S3
	//OUT = d_out_stolt_S3
	
	fftshift(d_out_stolt_S3, Nslow, Nfast);
	

	
	//-----------------------------------iFFT-----inplace----------------------------------------
	ifft(d_out_stolt_S3, Nslow, Nfast);


	//Normalizo la salida de i-FFT-- kernel propio in-place, piso de nuevo a d_out_stolt_S3 !!!!
	normalizarIfft(d_out_stolt_S3, Nslow, Nfast);


	//------------------------------------FFTSHIFT---------------------------------------------
        

	//obtener resultado final S4

	//para el caso de matrices cuadradas se cumple que i-FFTSHIFT = FFTSHIFT, lo hace in-place

	//piso in-place nuevamente a d_out_stolt_S3 
	//IN  = d_out_stolt_S3
	//OUT = d_out_stolt_S3
	fftshift(d_out_stolt_S3, Nslow, Nfast);



	//nuevo
	cuComplex *h_out_stolt_S3 =  (cuComplex *) malloc( dimx*dimy * sizeof(cuComplex));  
	
	if(LAB){
		//printf("init_mat_host  : \n");
		//getchar();
		init_mat_host(h_out_stolt_S3, Nslow, Nfast);
	
	}
	if(TIME){

		struct timeval mathoststart, mathostend;
		gettimeofday(&mathoststart, NULL);

		init_mat_host(h_out_stolt_S3, Nslow, Nfast);

		gettimeofday(&mathostend, NULL);
		printf("tiempo init_mat_host miliseg:%ld\n", ((mathostend.tv_sec * 1000 + mathostend.tv_usec/1000) - (mathoststart.tv_sec * 1000 + mathoststart.tv_usec/1000)));

	}


	//piso inplace a h_out_stolt_S3 !!!!!!
	checkCudaErrors( cudaMemcpy(h_out_stolt_S3, d_out_stolt_S3, Nslow*Nfast * sizeof(cuComplex), cudaMemcpyDeviceToHost) );

	//----------------------------------------------

	//escribir en archivo S4=h_out_stolt_S3

	if(WRITE)
	{

		char re_S4_file_debug[] = "re_S4_cuda7.txt";
		char im_S4_file_debug[] = "im_S4_cuda7.txt";

		if(LAB)
	        {
			saveMat_Parts(h_out_stolt_S3,Nslow, Nfast, re_S4_file_debug, im_S4_file_debug );
		}

		if(TIME){

			struct timeval Escriturastart, Escrituraend;
			gettimeofday(&Escriturastart, NULL);

			saveMat_Parts(h_out_stolt_S3,Nslow, Nfast, re_S4_file_debug, im_S4_file_debug );

			gettimeofday(&Escrituraend, NULL);
			printf("tiempo escritura S4 milisegundos:<%ld>\n", 
			((Escrituraend.tv_sec * 1000 + Escrituraend.tv_usec/1000) - (Escriturastart.tv_sec * 1000 + Escriturastart.tv_usec/1000)));

		}

	}

	//------------------------------------Limpieza ----------------------------------------------

	// d_out_stolt_S3, solo se eliminara si no se necesita para la validacion
	
	free(h_out_stolt_S3);
	checkCudaErrors( cudaFree(d_out_stolt_S3) );

	cudaDeviceReset();

	return 0;

}








