# Algoritmo WK - Version CPU para Cluster con MPI y Manycore con OpenMP, instrumentada para ARDUINO

El proposito de esta versión es la medición del consumo de Potencia de Wk_MPI_OMP. 
Se utiliza un hardware propio: Amperímetro basando en ARDUINO UNO y el Sensor 
de efecto Hall: ACS712.  

En esta versión se instrumenta la interpolación de Stolt.  

El algoritmo WK fue Creado en 1991 por Cafforio, Prati y Rocca. Es uno de los
algoritmos actuales más precisos comparado con RDA (Range Doppler Algorithm) y
CSA (Chirp Scaling Algorithm), realiza la focalización de la imagen SAR 
(randar de apertura sintetica) trabajando íntegramente en el dominio bidimensional 
de las frecuencias y de allí su nombre, W:frecuencia en Rango y K: frecuencia en acimut. 
Sus principales ventajas es que puede manejar grandes aperturas sintéticas 
o elevados ángulos de Squint. 

## Descripción del algoritmo WK:

 Entrada: datos crudos simulados SAR

* ifftshit
* fft2D
* iffshift
* compresion gruesa: RFM
* compresion diferencial: Interpolacion de Stolt ( OpenMP + MPI + Instrumentacion ARDUINO)
* ifft2D

Salida: Imagen focalizada

## Compilacion:

```
mpicc  wk_MPI_OMP_2018_ardu.c -lfftw3_omp -lfftw3 -lm -fopenmp -o wk_MPI_OMP_2018_ardu -DNslow=6144 -DNfast=6144 -DTHREADS=4 -DTIME=0 -DLAB=1
```
```
mpirun -np 2 --hostfile hostfilehome ./wk_MPI_OMP_2018_ardu 
```

## Autor

* **javier nicolas uranga: javiercba@gmail.com**

## Licencia

Este proyecto esta bajo lincencia GPLv3