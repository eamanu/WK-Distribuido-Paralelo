# Algoritmo WK - Versión para Cluster de GPU: Tegra K1 con MPI

El algoritmo WK fue Creado en 1991 por Cafforio, Prati y Rocca. Es uno de los
algoritmos actuales más precisos comparado con RDA (Range Doppler Algorithm) y
CSA (Chirp Scaling Algorithm), realiza la focalización de la imagen SAR 
(randar de apertura sintetica) trabajando íntegramente en el dominio bidimensional 
de las frecuencias y de allí su nombre, W:frecuencia en Rango y K: frecuencia en acimut. 
Sus principales ventajas es que puede manejar grandes aperturas sintéticas 
o elevados ángulos de Squint.  

## Descripción del algoritmo WK:

 Entrada: datos crudos simulados SAR

* ifftshit
* fft2D
* iffshift
* compresion gruesa: RFM
* compresion diferencial: Interpolacion de Stolt (Esta funcion se implementa con MPI+CUDA)
* ifft2D

Salida: Imagen focalizada

## Compilacion:

```
nvcc -I /home/ubuntu/.openmpi/include/  -L /home/ubuntu/.openmpi/lib/ -lmpi  -lcufft  wk_cuMPI_spa.cu -o wk_cuMPI_spa  -DNslow=6144 -DNfast=6144
```
```
mpirun -np 2 --hostfile hostfilehome ./wk_cuMPI_spa 
```

## Autor

* **javier nicolas uranga: javiercba@gmail.com**

## Licencia

Este proyecto esta bajo lincencia GPLv3